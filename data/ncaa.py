import requests, re, os
import threading

def safe_make_dir(path):
    try:
        os.makedirs(path)
        return True
    except Exception, e:
        return False

def get_year(year, idx = 0, cookies = {}):
    idx_filename = os.path.join(str(year),'index.txt')
    if os.path.exists(idx_filename):
        with open(idx_filename,'r') as f:
            lns = f.readlines()
            return [w.strip() for w in lns]

    if idx ==0:
        print 'Requesting Year: %d Page: %d' % (year,idx),
    else:
        print ' ',idx,
    data = {}
    if idx == 0:
        data={
            'doWhat': 'teamSearch',
                'searchOrg': 'X',
                'academicYear': year,
                'searchSport': 'MBB',
                'searchDiv': '1'
        }
    else:
        data= {
            'sortOn':'0',
            'idx': idx,
            'doWhat': 'showIdx'
        }
    r = requests.post('http://web1.ncaa.org/stats/StatsSrv/careersearch', cookies=cookies, data = data)
    if r.status_code != 200:
        print 'Could not get year %d ' % year
        return []

    id_ptrn = re.compile('showTeamPage\\(.*,\s?(\d+)\\)')
    teamIds = id_ptrn.findall(r.text)
    if idx > 0:
        return teamIds

    # Recursively load the other pages
    mp_ptrn = re.compile('showNext')
    addt_pages = len(mp_ptrn.findall(r.text)) - 1
    for x in xrange(1,addt_pages):
        err=False
        try:
            teamIds.extend(get_year(year, idx=x, cookies = r.cookies))
        except Exception, e:
            print 'Could not get  %d page %d' % (year,x)
            err=True
        if err:
            break

    safe_make_dir(str(year))
    with open(idx_filename,'w') as f:
        for id in teamIds:
            f.write(id)
            f.write('\n')
    print '\n',
    return teamIds


def get_team_html(year, id):
    dirname = os.path.join(str(year),str(id))
    filename = os.path.join(dirname,'complete.html')
    if os.path.exists(filename):
        print ".", # cached
        return
    print "+", # get new
    r = requests.post('http://web1.ncaa.org/stats/StatsSrv/careerteam',allow_redirects=False, data = {
        'sortOn':'0',
        'doWhat': 'display',
        'playerId': '-100',
        'coachId': '-100',
        'orgId': id,
        'academicYear': year,
        'division': '1',
        'sportCode': 'MBB',
        'idx': ''
    })
    if r.status_code != 200:
        print 'Could not get team %s' % id
        return

    safe_make_dir(dirname)
    with open(filename,'w') as f:
        f.write(r.text.encode('utf-8'))



if __name__ == '__main__':
    for yr in xrange(2015,2000,-1):
        ids = get_year(yr)

        print 'Getting %d teams' % len(ids)
        for id in ids:
            get_team_html(yr,id)
