Data 
=====
Python scripts for gathering data.

## Requirements
Python 2.7.x

Install the requests library
`pip install requests`
