import os
import argparse
import sys

class ArffReader(object):
    def __init__(self, file_name, line_func, attr_func):
        self.attrs=[]
        self.file_name=file_name
        self.line_func=line_func
        self.attr_func = attr_func
        self.read_arff()

    def read_arff(self):
        line_number = 0
        with open(self.file_name,'r') as f:
            print 'Reading file: {0}'.format(self.file_name),
            datasection=False
            try:
                for line in f.readlines():
                    line_number +=1
                    if line.find('%') == 0:
                        continue
                    if line.find('@attribute ') == 0:
                        p = line.split(' ')
                        self.attrs.append(p[1])
                        continue
                    if line.find('@data') == 0:
                        datasection=True
                        self.attrs = self.attr_func(self.attrs)
                        continue
                    if not datasection:
                        continue

                    self.line_func(line)
            except Exception, e:
                print 'Error on line : {0}\n{1}'.format(line_number, e.message)
        print '....done ({0} lines)'.format(line_number)


def make_key(year,team_a,team_b):
    if team_a < team_b:
        temp = team_a
        team_a = team_b
        team_b = temp
    return ':'.join([year,team_a,team_b])

def read_games(line):
    global data
    pieces=line.strip().split(',')
    if len(pieces) < 7:
        raise Exception('Missing a column')
    # key = year:Team1:Team2
    team_id=make_key(pieces[0], pieces[3], pieces[5])

    # ints=[pieces[2],pieces[4],pieces[6]]
    # data[team_id] =[ int(x) for x in ints ]
    scored = int(pieces[6])
    if pieces[3] < pieces[5]:
        scored *= -1
    wl =''
    if scored > 0:
        wl = 'w'
    elif scored == 0:
        wl = 't'
    else:
        wl = 'l'
    data[team_id] = [wl,scored]

def read_team_stats(line):
    global teams
    pieces = line.strip().split(',')
    name = pieces[0]
    if not name in teams:
        teams[name] = {}

    teams[name][pieces[1]] = [float(x) for x in pieces[2:26]]

def read_kenpom(line):
    global teams
    global missing_team
    global missing_team_years
    pieces = line.strip().split(',')
    name = pieces[0]
    if name not in teams:
        missing_team.add(name)
        return
        # raise Exception('Name not found in kenpom {0}'.format(name))
    year = pieces[-1]
    if year not in teams[name]:
        missing_team_years.add(name+':'+year)
        return
        # raise Exception('Missing {1} year for {0}:'.format(name,year))
    teams[name][year].extend([float(x) for x in pieces[1:-1]])

def join(data,teams):
    for key in data.keys():
        (year,team_a,team_b) = key.split(':')
        if team_a not in teams or year not in teams[team_a]:
            del data[key]
            continue
        if team_b not in teams or year not in teams[team_b]:
            del data[key]
            continue
        data[key].extend(teams[team_a][year])
        data[key].extend(teams[team_b][year])

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-k',required=True,dest='kenpom')
    parser.add_argument('-t',required=True,dest='team_stats')
    parser.add_argument('-g',required=True,dest='games')
    parser.add_argument('-o',required=True,dest='output')


    args = parser.parse_args()

    data={}
    teams={}
    missing_team=set()
    missing_team_years=set()

    games=ArffReader(args.games, read_games, lambda a: ['WinLoss',a[6]])
    team_stats=ArffReader(args.team_stats, read_team_stats, lambda a: a[2:26])
    kenpom=ArffReader(args.kenpom,read_kenpom, lambda a: a[1:-1])

    join(data, teams)
    attributes=[]
    attributes.extend(games.attrs)
    attributes.extend(['home_'+x for x in team_stats.attrs])
    attributes.extend(['home_'+x for x in kenpom.attrs])
    attributes.extend(['opp_'+x for x in team_stats.attrs])
    attributes.extend(['opp_'+x for x in kenpom.attrs])

    with open(args.output,'w') as f:
        f.write('@relation joined_team_states\n')
        for a in attributes:
            var_type = '{w,l}' if a == 'WinLoss' else 'real'
            f.write('@attribute {0} {1}\n'.format(a.replace("'",''),var_type))
        f.write('@data\n')
        # f.write(','.join(attributes))
        # f.write('\n')
        for (_,stats) in data.iteritems():
            if len(stats) != len(attributes):
                continue
            f.write(','.join([str(x) for x in stats[:(len(attributes))]]))
            f.write('\n')

    print 'Missing teams:'
    for l in missing_team:
        print l
    print 'Missing years:'
    for l in missing_team_years:
        print l
