/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parsebrackets;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Pattern;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 *
 * @author Michael
 */
public class ParseBrackets {

    /**
     * @param args the command line arguments
     */
    public void run(int bottomend) throws Exception
    {
        ArrayList<String> Round=new ArrayList<>();
        ArrayList<String> Teams=new ArrayList<>();
        ArrayList<Integer> Scores=new ArrayList<>();
        ArrayList<Integer> seeds=new ArrayList<>();
        ArrayList<Integer> years=new ArrayList<>();
        for(int i=2014; i>=bottomend; i--)
        {
            String fileName="C:\\Users\\Michael\\Documents\\cs478\\march-madness\\data\\"
                    +Integer.toString(i)+".html";
            Document doc=Jsoup.parse(new File(fileName), "utf-8");
            Element table=doc.getElementsByClass("search-results").first();
            Element body=table.getElementsByTag("tbody").first();
            for(Element game: body.getElementsByTag("tr"))
            {
                for(Element data: game.getElementsByTag("td"))
                {
                    if(data.className().equals("year"))
                    {
                        years.add(Integer.parseInt(data.getElementsByTag("a").first().text()));
                    }
                    else if(data.className().equals("round"))
                    {
                        Round.add(data.getElementsByClass("no-display").text());
                    }
                    else if(data.className().equals("seed"))
                    {
                        seeds.add(Integer.parseInt(data.text()));
                    }
                    else if(data.className().contains("team"))
                    {
                        Teams.add(data.getElementsByClass("no-display").first().text());
                    }
                    else if(data.className().contains("score"))
                    {
                        Scores.add(Integer.parseInt(data.text()));
                    }
                }
            }
        }
        FileOutputStream fos=new FileOutputStream(new File("mm.arff"));
        PrintWriter pw=new PrintWriter(fos);
        int teamPtr=0;
        int scorePtr=0;
        int seedPtr=0;
        int roundPtr=0;
        for(Integer yr: years)
        {
           pw.print(yr);
           pw.print(",");
           pw.print(Round.get(roundPtr));
           pw.print(",");
           roundPtr++;
           pw.print(seeds.get(seedPtr));
           pw.print(",");
           seedPtr++;
           pw.print(Teams.get(teamPtr));
           teamPtr++;
           pw.print(",");
           pw.print(seeds.get(seedPtr));
           pw.print(",");
           seedPtr++;
           pw.print(Teams.get(teamPtr));
           pw.print(",");
           int scoreDiff=Scores.get(scorePtr)-Scores.get(scorePtr+1);
           scorePtr+=2;
           pw.print(scoreDiff);
           teamPtr++;
           pw.print("\r\n");
        }
        pw.close();
        fos.close();
    }
    
    public static void main(String[] args) 
    {
        if(args.length<1)
        {
            System.out.println("Need an arg for where to go through on bottom end");
        }
        int bottomend=3;
        try
        {
            bottomend=Integer.parseInt(args[0]);
            new ParseBrackets().run(bottomend);
        }
        catch(Exception e)
        {
            System.out.println("An error occurred");
            e.printStackTrace();
        }
    }
    
}
