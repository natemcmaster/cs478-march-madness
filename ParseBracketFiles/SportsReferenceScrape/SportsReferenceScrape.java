/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sportsreferencescrape;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Pattern;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 *
 * @author Michael
 */
public class SportsReferenceScrape {

    public SportsReferenceScrape() throws IOException
    {
        this.fos = new FileOutputStream(new File("team-stats.arff"));
        pw=new PrintWriter(fos);
        schoolnames.put("St. Mary's (Cal.)", "saint-marys-ca");
        schoolnames.put("St. Louis", "saint-louis");
        schoolnames.put("St. Joseph's", "saint-josephs");
        schoolnames.put("North Carolina A&T", "north-carolina-at");
        schoolnames.put("Middle Tennessee State", "middle-tennessee");
        schoolnames.put("Long Island", "long-island-university");
        schoolnames.put("Miami (Fla.)", "miami-fl");
        schoolnames.put("Murray St.", "murray-state");
        schoolnames.put("Detroit", "detroit-mercy");
        schoolnames.put("UNC Asheville", "north-carolina-asheville");
        schoolnames.put("St. Peter's", "saint-peters");
        schoolnames.put("St. John's", "st-johns-ny");
        schoolnames.put("UC Santa Barbara", "california-santa-barbara");
        schoolnames.put("Miami (Ohio)", "miami-oh");
        schoolnames.put("UNC Wilmington", "north-carolina-wilmington");
    }

    /**
     * @param args the command line arguments
     */
    private void getTeamStats(String fileName, int year) throws IOException, NullPointerException
    {
        Document doc=Jsoup.connect(fileName).get();
        Element table=doc.getElementById("team_stats");
        Map<String, Double> thisTeamStats=new HashMap<>();
        Map<String, Double> thatTeamStats=new HashMap<>();
        Element tHead=table.getElementsByTag("thead").get(0).getElementsByTag("tr").get(0);
        Element teamOne=table.getElementsByTag("tbody").get(0).getElementsByTag("tr").get(0);
        Element teamTwo=table.getElementsByTag("tbody").get(0).getElementsByTag("tr").get(2);
        for(int i=1; i<tHead.children().size(); i++)
        {
            String key=tHead.child(i).text();
            try
            {
                thisTeamStats.put(key, Double.parseDouble(teamOne.child(i).text()));
            }
            catch(NumberFormatException e)
            {
                thisTeamStats.put(key, -1d);
            }
            try
            {
                thatTeamStats.put(key, Double.parseDouble(teamTwo.child(i).text()));
            }
            catch(NumberFormatException e)
            {
                thatTeamStats.put(key, -1d);
            }
        }
        StringBuilder sb=new StringBuilder();
        sb.append(this.currentSchool);
        sb.append(",");
        sb.append(year);
        sb.append(",");
        for(String stat: thisTeamStats.keySet())
        {
          Double statValue=thisTeamStats.get(stat);
          sb.append(statValue);
          sb.append(",");
        }
        for(String stat: thatTeamStats.keySet())
        {
          Double statValue=thatTeamStats.get(stat);
          sb.append(statValue);
          sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);
        pw.println(sb.toString());
    }
    
    private static String preProcess(String a)
    {
        a=a.toLowerCase();
        a=a.replaceAll("\\s", "-");
        a=a.replaceAll("\\.", "");
        a=a.replaceAll("\\(", "");
        a=a.replaceAll("\\)","");
        a=a.replaceAll("\\'", "");
        a=a.replaceAll("\\&","");
        return a;
    }
    
    private Map<String, String> schoolnames=new HashMap<>();
        
    private String currentSchool;
    
    private void convertAndGet(String schoolname, int year) throws IOException
    {
            currentSchool=schoolname;
            System.out.println(currentSchool+Integer.toString(year));
            if(schoolnames.containsKey(schoolname))
                schoolname=schoolnames.get(schoolname);
            else
                schoolname=preProcess(schoolname);
            String fileName;
            Scanner in=new Scanner(System.in);
            try
            {
                fileName="http://www.sports-reference.com/cbb/schools/"+schoolname+"/"+year+".html";
                getTeamStats(fileName, year);
                schoolnames.put(currentSchool, schoolname);
            }
            catch(IOException | NullPointerException e)
            {
                System.out.println("Could not find reference for: "+schoolname+". Please advise.");
                schoolnames.put(currentSchool, in.nextLine());
                schoolname=schoolnames.get(currentSchool);
                fileName="http://www.sports-reference.com/cbb/schools/"+schoolname+"/"+year+".html";
                getTeamStats(fileName, year);
            }
    }
    
    FileOutputStream fos;
    PrintWriter pw;
    
    private void run(String filename) throws FileNotFoundException, IOException
    {
        FileInputStream fis=new FileInputStream(new File(filename));
        BufferedInputStream bis=new BufferedInputStream(fis);
        Scanner scanner=new Scanner(bis);
        while(!scanner.nextLine().equals("@data"))
        {}
        scanner.useDelimiter(Pattern.compile(",|\n"));
        while(scanner.hasNextLine())
        {
            int year=-1;
            if(scanner.hasNextInt())
                year=scanner.nextInt();
            scanner.next();//round
            scanner.next();//seed
            String schoolname=scanner.next();
            this.convertAndGet(schoolname, year);
            scanner.next();//seed 2
            schoolname=scanner.next();
            this.convertAndGet(schoolname, year);
            scanner.next();//point differential
        }
        pw.close();
        fos.close();
    }
    
    public static void main(String[] args) throws FileNotFoundException, IOException
    {
        new SportsReferenceScrape().run(args[0]);
    }
    
}
